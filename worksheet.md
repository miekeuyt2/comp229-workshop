# Task 0

Clone this repository (well done!)

# Task 1

Take a look at the two repositories:

  * (A) https://bitbucket.org/altmattr/personalised-correlation/src/master/
  * (B) https://github.com/Whiley/WhileyCompiler

And answer the following questions about them:

  * These repositories are at two different websites - github and bitbucket - what are these sites?  What service do they provide? Which is better?
  BitBucket and GitHub are both Git version control things. They are both similar, but Bitbucket also is compatible with Mercurial and tends to be cheaper in terms of paid options.
  * Who made the last commit to repository A?
  Tam Du made the last commit to repository A, on the 29th July 2018.
  * Who made the first commit to repository A?
  The first commit in repository A was by Jon Mountjoy on the 11th August 2014.
  * Who made the first and last commits to repository B?
  Dave Pearce made both the first and last commits to repository B.
  * Are either/both of these projects active at the moment?  🤔 If not, what do you think happened?
  I would think that both of these projects are active at the moment, as they both have received commits within the last week.
  * 🤔 Which file in each project has had the most activity?
  Repository A's most edited file was index.html and app.py, and in repository B the most edited file was build.num.

# Task 2

Setup a new IntelliJ project with a main method that will print the following message to the console when run:

~~~~~
Sheep and Wolves
~~~~~